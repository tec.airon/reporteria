﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using word= Microsoft.Office.Interop.Word;



namespace reporteria
{
    public partial class frmventascred : Form
    {
        public void validarnum()
        {

        }
        public void reemplazar()
        {
            object ObjMiss = System.Reflection.Missing.Value;
            word.Application objword = new word.Application();
            word.Document objDoc = objword.Documents.Add(ref ObjMiss, ref ObjMiss, ref ObjMiss, ref ObjMiss);
            objDoc.Activate();
            objword.Selection.Font.Color = word.WdColor.wdColorBlack;
            string fecha = DateTime.Now.ToShortDateString();
            string cuotas = nudcuotas.Value.ToString();
            string titulo = "CONTRATO DE COMPRA VENTA A CRÉDITO";
            objword.Selection.BoldRun();
            objword.Selection.TypeText(titulo);
            string cuerpo = " Conste por el presente documento, que celebran de una parte," +
                " EL ENCUENTRO MUEBLERIA Y FERRETERIA, con NRC 252763 - 6, con domicilio en " +
                "calle cárdenas colonia milagro de la paz, zona alta, san miguel, en adelante se " +
                "denominará LA EMPRESA, y de otra parte por el/ la Sr / a: ";
            
            objword.Selection.TypeText(cuerpo + txtnombre.Text);
            string documento =" Con Documento: ";
            objword.Selection.TypeText(documento + txtdocnum.Text);
            string numeros = " Número: cero tres millones doscientos treinta y tres mil ochocientos catorce guion seis";
            objword.Selection.TypeText(numeros);
            string emitido = " emitido en: ";
            objword.Selection.TypeText(emitido + cmbemitido.Text);
            string alos = "a los: ";
            objword.Selection.TypeText(alos + fecha);
            string resto = " a quien en adelante se le denominara EL CLIENTE los términos y condición siguientes" +
            " Primera. - LA EMPRESA, es una Institución que se dedica a la comercialización de Muebles;" +
            " Electrodomésticos, Herramientas, y otros insumos de ferretería," +
            " Segundo. - Por el presente contrato LA EMPRESA da a EL CLIENTE en venta: Cama marca: Pérez modelo: 1.20" +
            " Tercero. - LA EMPRESA y EL CLIENTE de común acuerdo establecen que la presente venta es al crédito y que " +
            " el precio pactado es de $138(cuarenta y seis / 100 dólares americanos) incluido el IVA, que EL CLIENTE " +
            " pagará en ";
            objword.Selection.TypeText(cuotas);
            string despues ="cuotas mensuales de $46(Ciento cuarenta y seis / 100.Dólares Americanos) cada una, monto que " +
            " será facturado de manera mensual por medio de un recibo(la factura emitida acreditará al cliente el pago " +
            " de su letra y servirá como sustento de cancelación).Habiendo aportado como una prima la cantidad de:" +
            " $46(cuarenta y seis / 100 dólares americanos) El pago de estas cuotas se efectuará en él local de LA EMPRESA" +
            " Cuarto. - A efectos de garantizar el cumplimiento de su obligación EL CLIENTE gira a favor de LA EMPRESA 1(UNA) " +
            " letra de cambio por el valor del precio pactado en la cláusula tercera más intereses a la fecha de vencimiento, " +
            " incluyendo gastos administrativos LA EMPRESA al término" +
            " de su obligación contractual con el cliente, efectuará la devolución de la letra de cambio debidamente cancelada," +
            " asimismo las partes acuerdan que si EL CLIENTE" +
            " incumpliera con la cláusula quinta el cambial(letra) podrá ser protestada y ejecutada por" +
            " la vía legal." +
            " Quinto. - Si EL CLIENTE, incumpliera en el pago de dos cuotas o cuatro alternadas" +
            " LA EMPRESA hará efectiva la letra de cambio de acuerdo a las cláusulas cuarta y" +
            " décima cuarta del presente contrato.EL CLIENTE queda estrictamente prohibido" +
            " de extraer o cambiar, añadir, alterar o cualquier otra forma.modificar equipo electrónico" +
            " como televisores, computadoras, teléfonos, quedando bajo" +
            " su responsabilidad cualquier obligación que genere por este motivo: si" +
            " LA EMPRESA detecta cualquier situación anómala de cualquiera de estas" +
            " circunstancias dará por terminado de" +
            " inmediato el periodo de garantía sin responsabilidad, ejecutando la cláusula cuarta." +
            " Sexto. - Las partes acuerdan que para todo lo relacionado con el cumplimiento de las" +
            " cláusulas" +
            " del presente contrato de compra venta, las partes acuerdan someterse a la jurisdicción de" +
            " los jueces y tribunales de san miguel, o mediante arbitraje, señalando sus domicilios tales" +
            " •consignados en la introducción del presente contrato" +
            " Contacto del cliente 6052 - 0169";
            
            //string footer=" LA EMPRESA  EL CLIENTE";
            objword.Selection.TypeText(resto) ;
            objword.Selection.TypeText(despues) ;
            objword.Selection.TypeText(txtelefono.Text + ' ');
            objword.Selection.TypeText(txtotal.Text + ' ');
            objword.Selection.TypeText(txtprima.Text + ' ');
            
            
            objword.Visible = true;




        }
        public void reemplazar2()
        {
            
        }
        

        public frmventascred()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Desea salir del formulario", "GestiPlus", MessageBoxButtons.YesNo))
            {
                this.Close();
            }

        }

        private void txtdocnum_TextChanged(object sender, EventArgs e)
        {
            
          

        }

        private void txtdocnum_KeyPress(object sender, KeyPressEventArgs e)
        {
            

            // only allow one decimal point
           // if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
             //   e.Handled = true;
            //}
        }

        private void txtelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
               (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtprima_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
               (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtprima_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void frmventascred_Load(object sender, EventArgs e)
        {

        }

        private void btngenerar_Click(object sender, EventArgs e)
        {
            reemplazar();

         }
    }
}
