﻿namespace reporteria
{
    partial class frmventascred
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnombre = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.txtdocnum = new System.Windows.Forms.TextBox();
            this.lbldoc = new System.Windows.Forms.Label();
            this.dtpfechaext = new System.Windows.Forms.DateTimePicker();
            this.lblfext = new System.Windows.Forms.Label();
            this.txtidproducto = new System.Windows.Forms.TextBox();
            this.lblidprod = new System.Windows.Forms.Label();
            this.btnf1 = new System.Windows.Forms.Button();
            this.lblcrearexp = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.nudCant = new System.Windows.Forms.NumericUpDown();
            this.lblcantidad = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbltotal = new System.Windows.Forms.Label();
            this.txtotal = new System.Windows.Forms.TextBox();
            this.lblcuotas = new System.Windows.Forms.Label();
            this.gbdatoscliente = new System.Windows.Forms.GroupBox();
            this.cmbemitido = new System.Windows.Forms.ComboBox();
            this.txtelefono = new System.Windows.Forms.TextBox();
            this.lbltelefono = new System.Windows.Forms.Label();
            this.gbproductos = new System.Windows.Forms.GroupBox();
            this.nudcuotas = new System.Windows.Forms.NumericUpDown();
            this.lblprima = new System.Windows.Forms.Label();
            this.txtprima = new System.Windows.Forms.TextBox();
            this.gbcompromiso = new System.Windows.Forms.GroupBox();
            this.btngenerar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.lblemitido = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudCant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbdatoscliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudcuotas)).BeginInit();
            this.SuspendLayout();
            // 
            // lblnombre
            // 
            this.lblnombre.AutoSize = true;
            this.lblnombre.Location = new System.Drawing.Point(44, 66);
            this.lblnombre.Name = "lblnombre";
            this.lblnombre.Size = new System.Drawing.Size(44, 13);
            this.lblnombre.TabIndex = 0;
            this.lblnombre.Text = "Nombre";
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(94, 63);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(373, 20);
            this.txtnombre.TabIndex = 1;
            // 
            // txtdocnum
            // 
            this.txtdocnum.Location = new System.Drawing.Point(94, 89);
            this.txtdocnum.Name = "txtdocnum";
            this.txtdocnum.Size = new System.Drawing.Size(131, 20);
            this.txtdocnum.TabIndex = 2;
            this.txtdocnum.TextChanged += new System.EventHandler(this.txtdocnum_TextChanged);
            this.txtdocnum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdocnum_KeyPress);
            // 
            // lbldoc
            // 
            this.lbldoc.AutoSize = true;
            this.lbldoc.Location = new System.Drawing.Point(33, 96);
            this.lbldoc.Name = "lbldoc";
            this.lbldoc.Size = new System.Drawing.Size(55, 13);
            this.lbldoc.TabIndex = 3;
            this.lbldoc.Text = "Num Doc.";
            // 
            // dtpfechaext
            // 
            this.dtpfechaext.Location = new System.Drawing.Point(94, 125);
            this.dtpfechaext.MinDate = new System.DateTime(2021, 11, 1, 0, 0, 0, 0);
            this.dtpfechaext.Name = "dtpfechaext";
            this.dtpfechaext.Size = new System.Drawing.Size(200, 20);
            this.dtpfechaext.TabIndex = 4;
            // 
            // lblfext
            // 
            this.lblfext.Location = new System.Drawing.Point(33, 125);
            this.lblfext.Name = "lblfext";
            this.lblfext.Size = new System.Drawing.Size(55, 34);
            this.lblfext.TabIndex = 5;
            this.lblfext.Text = "Fecha Extension";
            // 
            // txtidproducto
            // 
            this.txtidproducto.Location = new System.Drawing.Point(94, 196);
            this.txtidproducto.Name = "txtidproducto";
            this.txtidproducto.Size = new System.Drawing.Size(45, 20);
            this.txtidproducto.TabIndex = 6;
            // 
            // lblidprod
            // 
            this.lblidprod.Location = new System.Drawing.Point(33, 199);
            this.lblidprod.Name = "lblidprod";
            this.lblidprod.Size = new System.Drawing.Size(55, 18);
            this.lblidprod.TabIndex = 7;
            this.lblidprod.Text = "Producto";
            // 
            // btnf1
            // 
            this.btnf1.Location = new System.Drawing.Point(145, 196);
            this.btnf1.Name = "btnf1";
            this.btnf1.Size = new System.Drawing.Size(36, 23);
            this.btnf1.TabIndex = 8;
            this.btnf1.Text = "[F1]";
            this.btnf1.UseVisualStyleBackColor = true;
            // 
            // lblcrearexp
            // 
            this.lblcrearexp.AutoSize = true;
            this.lblcrearexp.Location = new System.Drawing.Point(211, 28);
            this.lblcrearexp.Name = "lblcrearexp";
            this.lblcrearexp.Size = new System.Drawing.Size(161, 13);
            this.lblcrearexp.TabIndex = 9;
            this.lblcrearexp.Text = "Crear expediente de compromiso";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBox1.Location = new System.Drawing.Point(194, 199);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(273, 20);
            this.textBox1.TabIndex = 10;
            // 
            // nudCant
            // 
            this.nudCant.Location = new System.Drawing.Point(534, 200);
            this.nudCant.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCant.Name = "nudCant";
            this.nudCant.Size = new System.Drawing.Size(37, 20);
            this.nudCant.TabIndex = 11;
            this.nudCant.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblcantidad
            // 
            this.lblcantidad.Location = new System.Drawing.Point(473, 202);
            this.lblcantidad.Name = "lblcantidad";
            this.lblcantidad.Size = new System.Drawing.Size(55, 18);
            this.lblcantidad.TabIndex = 12;
            this.lblcantidad.Text = "Cantidad";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(36, 232);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(535, 150);
            this.dataGridView1.TabIndex = 13;
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.Location = new System.Drawing.Point(421, 402);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(31, 13);
            this.lbltotal.TabIndex = 14;
            this.lbltotal.Text = "Total";
            // 
            // txtotal
            // 
            this.txtotal.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtotal.Enabled = false;
            this.txtotal.Location = new System.Drawing.Point(471, 395);
            this.txtotal.Name = "txtotal";
            this.txtotal.Size = new System.Drawing.Size(100, 20);
            this.txtotal.TabIndex = 15;
            // 
            // lblcuotas
            // 
            this.lblcuotas.AutoSize = true;
            this.lblcuotas.Location = new System.Drawing.Point(35, 477);
            this.lblcuotas.Name = "lblcuotas";
            this.lblcuotas.Size = new System.Drawing.Size(40, 13);
            this.lblcuotas.TabIndex = 16;
            this.lblcuotas.Text = "Cuotas";
            // 
            // gbdatoscliente
            // 
            this.gbdatoscliente.Controls.Add(this.lblemitido);
            this.gbdatoscliente.Controls.Add(this.cmbemitido);
            this.gbdatoscliente.Controls.Add(this.txtelefono);
            this.gbdatoscliente.Controls.Add(this.lbltelefono);
            this.gbdatoscliente.Location = new System.Drawing.Point(28, 44);
            this.gbdatoscliente.Name = "gbdatoscliente";
            this.gbdatoscliente.Size = new System.Drawing.Size(452, 129);
            this.gbdatoscliente.TabIndex = 18;
            this.gbdatoscliente.TabStop = false;
            this.gbdatoscliente.Text = "Datos Cliente";
            // 
            // cmbemitido
            // 
            this.cmbemitido.FormattingEnabled = true;
            this.cmbemitido.Items.AddRange(new object[] {
            "Centro San Salvador",
            "Soyapango",
            "Centro Comercial Galerias, San Salvador:",
            "Mejicanos:",
            "Apopa",
            "Santa Tecla",
            "Lourdes, Colón",
            "Cojutepeque",
            "Zacatecoluca",
            "San Vicente",
            "Chalatenango",
            "Santa Ana",
            "Sonsonate",
            "Sensuntepeque",
            "Ahuachapán",
            "Santiago de María",
            "Usulután",
            "San Miguel",
            "San Francisco Gotera",
            "La Unión"});
            this.cmbemitido.Location = new System.Drawing.Point(318, 81);
            this.cmbemitido.Name = "cmbemitido";
            this.cmbemitido.Size = new System.Drawing.Size(121, 21);
            this.cmbemitido.TabIndex = 6;
            // 
            // txtelefono
            // 
            this.txtelefono.Location = new System.Drawing.Point(308, 45);
            this.txtelefono.Name = "txtelefono";
            this.txtelefono.Size = new System.Drawing.Size(131, 20);
            this.txtelefono.TabIndex = 5;
            this.txtelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtelefono_KeyPress);
            // 
            // lbltelefono
            // 
            this.lbltelefono.AutoSize = true;
            this.lbltelefono.Location = new System.Drawing.Point(240, 48);
            this.lbltelefono.Name = "lbltelefono";
            this.lbltelefono.Size = new System.Drawing.Size(49, 13);
            this.lbltelefono.TabIndex = 4;
            this.lbltelefono.Text = "Telefóno";
            // 
            // gbproductos
            // 
            this.gbproductos.Location = new System.Drawing.Point(28, 179);
            this.gbproductos.Name = "gbproductos";
            this.gbproductos.Size = new System.Drawing.Size(543, 251);
            this.gbproductos.TabIndex = 19;
            this.gbproductos.TabStop = false;
            this.gbproductos.Text = "Productos";
            // 
            // nudcuotas
            // 
            this.nudcuotas.Location = new System.Drawing.Point(81, 475);
            this.nudcuotas.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudcuotas.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudcuotas.Name = "nudcuotas";
            this.nudcuotas.Size = new System.Drawing.Size(46, 20);
            this.nudcuotas.TabIndex = 20;
            this.nudcuotas.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // lblprima
            // 
            this.lblprima.AutoSize = true;
            this.lblprima.Location = new System.Drawing.Point(159, 477);
            this.lblprima.Name = "lblprima";
            this.lblprima.Size = new System.Drawing.Size(33, 13);
            this.lblprima.TabIndex = 22;
            this.lblprima.Text = "Prima";
            // 
            // txtprima
            // 
            this.txtprima.Location = new System.Drawing.Point(198, 476);
            this.txtprima.Name = "txtprima";
            this.txtprima.Size = new System.Drawing.Size(131, 20);
            this.txtprima.TabIndex = 23;
            this.txtprima.TextChanged += new System.EventHandler(this.txtprima_TextChanged);
            this.txtprima.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprima_KeyPress);
            // 
            // gbcompromiso
            // 
            this.gbcompromiso.Location = new System.Drawing.Point(28, 447);
            this.gbcompromiso.Name = "gbcompromiso";
            this.gbcompromiso.Size = new System.Drawing.Size(316, 71);
            this.gbcompromiso.TabIndex = 25;
            this.gbcompromiso.TabStop = false;
            this.gbcompromiso.Text = "Terminos compromiso";
            // 
            // btngenerar
            // 
            this.btngenerar.Location = new System.Drawing.Point(424, 545);
            this.btngenerar.Name = "btngenerar";
            this.btngenerar.Size = new System.Drawing.Size(75, 23);
            this.btngenerar.TabIndex = 26;
            this.btngenerar.Text = "Generar";
            this.btngenerar.UseVisualStyleBackColor = true;
            this.btngenerar.Click += new System.EventHandler(this.btngenerar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.Location = new System.Drawing.Point(522, 545);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(75, 23);
            this.btnsalir.TabIndex = 27;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // lblemitido
            // 
            this.lblemitido.AutoSize = true;
            this.lblemitido.Location = new System.Drawing.Point(268, 84);
            this.lblemitido.Name = "lblemitido";
            this.lblemitido.Size = new System.Drawing.Size(44, 13);
            this.lblemitido.TabIndex = 7;
            this.lblemitido.Text = "Emitido:";
            // 
            // frmventascred
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 580);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btngenerar);
            this.Controls.Add(this.txtprima);
            this.Controls.Add(this.lblprima);
            this.Controls.Add(this.nudcuotas);
            this.Controls.Add(this.lblcuotas);
            this.Controls.Add(this.txtotal);
            this.Controls.Add(this.lbltotal);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblcantidad);
            this.Controls.Add(this.nudCant);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblcrearexp);
            this.Controls.Add(this.btnf1);
            this.Controls.Add(this.lblidprod);
            this.Controls.Add(this.txtidproducto);
            this.Controls.Add(this.lblfext);
            this.Controls.Add(this.dtpfechaext);
            this.Controls.Add(this.lbldoc);
            this.Controls.Add(this.txtdocnum);
            this.Controls.Add(this.txtnombre);
            this.Controls.Add(this.lblnombre);
            this.Controls.Add(this.gbdatoscliente);
            this.Controls.Add(this.gbproductos);
            this.Controls.Add(this.gbcompromiso);
            this.Name = "frmventascred";
            this.Text = "expediente de compromiso de pago";
            this.Load += new System.EventHandler(this.frmventascred_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudCant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbdatoscliente.ResumeLayout(false);
            this.gbdatoscliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudcuotas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblnombre;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.TextBox txtdocnum;
        private System.Windows.Forms.Label lbldoc;
        private System.Windows.Forms.DateTimePicker dtpfechaext;
        private System.Windows.Forms.Label lblfext;
        private System.Windows.Forms.TextBox txtidproducto;
        private System.Windows.Forms.Label lblidprod;
        private System.Windows.Forms.Button btnf1;
        private System.Windows.Forms.Label lblcrearexp;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown nudCant;
        private System.Windows.Forms.Label lblcantidad;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.TextBox txtotal;
        private System.Windows.Forms.Label lblcuotas;
        private System.Windows.Forms.GroupBox gbdatoscliente;
        private System.Windows.Forms.TextBox txtelefono;
        private System.Windows.Forms.Label lbltelefono;
        private System.Windows.Forms.GroupBox gbproductos;
        private System.Windows.Forms.NumericUpDown nudcuotas;
        private System.Windows.Forms.Label lblprima;
        private System.Windows.Forms.TextBox txtprima;
        private System.Windows.Forms.GroupBox gbcompromiso;
        private System.Windows.Forms.Button btngenerar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.ComboBox cmbemitido;
        private System.Windows.Forms.Label lblemitido;
    }
}

